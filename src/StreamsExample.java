import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class StreamsExample {
    public static void main(String[] args){
        getAverage();
        upperLower();
    }

    public static void getAverage(){
        List<Integer> numbers = Arrays.asList(1,2,3,4,5,6,7,8,9,10,11,12);
        double averageVal = numbers.stream().mapToDouble(Integer::doubleValue).average().orElse(0.0);
        System.out.println("Average value:" +averageVal);
    }
    
    public static void upperLower(){
        List<String > names = Arrays.asList("Shalika","bUddHini","ChampKa","hEwawAsam");
        List<String> upperValue = names.stream().map(name -> name.toUpperCase()).collect(Collectors.toList());
        List<String> lowerValue= names.stream().map(name -> name.toLowerCase()).collect(Collectors.toList());
        System.out.println("Uppsercase value:" +upperValue);
        System.out.println("Lowercase value:" +lowerValue);
    }
}
